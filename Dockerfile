FROM openjdk:11

ARG RESOURCE_PATH=./resources

ARG CREATED_USER=ciuser

ARG APACHE_VERSION=9.0.68

ARG FORTIFY_SSC_HOME=/opt/ssc
ARG SSC_VERSION=22.2.0
 

# "${RESOURCES_PATH}/customcacerts",
COPY [ "${RESOURCE_PATH}/fortify-ssc", "/root/" ]

# Get Packages / Binaries dependencies
ARG PKGREGISTRY_JOB_TOKEN
ARG PRIVATE_TOKEN=$PKGREGISTRY_JOB_TOKEN
ARG TOKEN_TYPE=JOB-TOKEN

RUN wget --header "${TOKEN_TYPE}:${PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/36628643/packages/generic/fortify-ssc/${SSC_VERSION}/ssc.war" -P /root/
RUN wget --header "${TOKEN_TYPE}:${PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/36628643/packages/generic/apache-tomcat/${APACHE_VERSION}/apache-tomcat-${APACHE_VERSION}.zip" -P /root/


RUN export DEBIAN_FRONTEND=noninteractive                                                               && \
    adduser --disabled-password --shell /bin/bash --home /home/${CREATED_USER} ${CREATED_USER}          && \
    apt-get update                                                                                      && \
    apt-get install unzip                                                                               && \
    mkdir -p ${FORTIFY_SSC_HOME}                                                                        && \
    unzip /root/apache-tomcat-${APACHE_VERSION}.zip -d /root/                                           && \
    mv /root/apache-tomcat-${APACHE_VERSION} ${FORTIFY_SSC_HOME}                                        && \
    chmod +x ${FORTIFY_SSC_HOME}/apache-tomcat-${APACHE_VERSION}/bin/*.sh                               && \
    cp -r /root/sqldriver/* ${FORTIFY_SSC_HOME}/apache-tomcat-${APACHE_VERSION}/lib                     && \
    mkdir -p ${FORTIFY_SSC_HOME}/apache-tomcat-${APACHE_VERSION}/webapps                                && \
    rm -rf ${FORTIFY_SSC_HOME}/apache-tomcat-${APACHE_VERSION}/webapps/ROOT/                            && \
    mv /root/ssc.war ${FORTIFY_SSC_HOME}/apache-tomcat-${APACHE_VERSION}/webapps/ROOT.war               && \
    chown ${CREATED_USER} -R ${FORTIFY_SSC_HOME}                                                        && \       
    # Add Entrypoints
    mkdir -p /entrypoints                                                                               && \
    mv /root/docker-entrypoint.sh /entrypoints                                                          && \
    chmod +x /entrypoints/*                                                                             && \
    chown ${CREATED_USER} -R /entrypoints                                                               && \
# # Certif
# chown -R ${CREATED_USER} /root/customcacerts                                                        && \
# cp /root/customcacerts $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts        && \
# chmod +r $(readlink -f /usr/bin/java | sed "s:bin/java::")lib/security/cacerts                      && \
    # Give rights and clean
    chown ${CREATED_USER} -R /opt/*                                                                     && \
    rm -r /root/* 



WORKDIR /home/${CREATED_USER}
USER ${CREATED_USER}

ENV USER_HOME=/home/${CREATED_USER}                                                                      \
    FORTIFY_SSC_HOME=${FORTIFY_SSC_HOME}                                                                 \
    APACHE_VERSION=${APACHE_VERSION}                                                                     \
    PATH=${FORTIFY_SSC_HOME}/apache-tomcat-${APACHE_VERSION}/bin:${PATH}

EXPOSE 8080

ENTRYPOINT [ "/entrypoints/docker-entrypoint.sh" ]
