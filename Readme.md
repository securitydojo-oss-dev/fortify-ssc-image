# Build Fortify Software Security Center image

Follow the instructions below to build this image :
Push the Fortify Software Security Center (SSC) .war file to your package registry.
If you add your keystore, uncomment the keystore copying in the Dockerfiles

### To push a package in your registry
using CURL
````curl -v --header "PRIVATE-TOKEN: [TOKEN]]" --upload-file ./ssc.war "https://gitlab.com/api/v4/projects/{​​$PROJECT}​​/{​​$VERSION}​​/{​​$FILENAME}​​?status=default" ````

## Image creation instructions
### Add/Generate missing files
If you need to use your own certificates you can add you cacerts to the resources folder.
Add your database sql driver to resources/sqldriver
### Build images
Type de command
````docker build -t fortify-ssc:{​​$VERSION}​​ -f Dockerfile .````

## Deployment
Here is a list of environment variables and empty volumes which are needed to deploy containers
/!\ All Tokens and passwords must be deployed as Kubernetes Secrets in the same namspace as the pods you are deploying /!
Some Tokens may already be created (such as CLIENT_AUTH_TOKEN, WORKER_AUTH_TOKEN, SSC_AUTH_TOKEN)
fortify-software-security-center:
- CA_CERTS_PASSWORD: Password of the Java KeyStore in which you must have imported your CA certificate  

You also need to persist volumes:
- /opt/ssc/apache-tomcat-9.0.45/logs
- /opt/ssc/apache-tomcat-9.0.45/conf - You need to mount your tomcat conf
- /home/ciuser/.fortify

## Annex

### Create a KeyStore and import the CA certificate into the KeyStore
Create a keystore with a strong password  
````keytool -genkey -v -keystore customcacerts -alias customCACerts -keyalg RSA -keysize 4096 -validity 10000````  
Then import the CA certificate (which it is named XXXX_CA_APPLICATIONS.cer)  
````keytool -importcert -file <CA_CERTIFICATE.cer> -alias <CA_NAME> -keystore customcacerts -trustcacerts````  